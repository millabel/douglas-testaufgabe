var gulp = require('gulp'),
	concat = require('gulp-concat'),
	compass = require('gulp-compass'),
	browserify = require('gulp-browserify');

var jsSources = [
	'components/scripts/*.js'
];

var sassSources = [
	'components/sass/style.scss'
];

gulp.task ('js', function() {
	gulp.src(jsSources)
		.pipe(concat('script.js'))
		//.pipe(browserify())
		.pipe(gulp.dest('builds/development/js'))
});

gulp.task ('compass', function() {
	gulp.src(sassSources)
		.pipe(compass({
			css: 'builds/development/css',
			sass: 'components/sass',
			image: 'builds/development/images',
			style: 'expanded'
		}))
		.pipe(gulp.dest('builds/development/css'))
});